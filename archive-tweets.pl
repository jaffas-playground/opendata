#!/usr/bin/perl
#
# Dump a user's tweets as individual JSON items
#
# Copyright (c) Andrew Flegg 2012. Released under the Artistic Licence
#
# Syntax:
#    archive-tweets <user> <directory>

use strict;
use warnings;
use Net::Twitter::Lite::WithAPIv1_1;
use JSON::XS;
use Date::Parse;
use POSIX qw(strftime);

our $USER = lc($ARGV[0]) or die "syntax: archive-tweets <user> <directory>\n";
our $DIR  = $ARGV[1] or die "syntax: archive-tweets $USER <directory>\n";
$| = 1;

my %setup = ( ssl => 1);
my $authFile = "$ENV{HOME}/.archive-tweets-keys.$USER";
if (-f $authFile) {
  my @cred = map { chomp; $_ } `cat '$authFile'`;
  $setup{consumer_key} = $cred[0];
  $setup{consumer_secret} = $cred[1];
  $setup{access_token} = $cred[2] if $cred[2];
  $setup{access_token_secret} = $cred[3] if $cred[3];
} else {
  $| = 1;
  print "Enter consumer key: ";
  $setup{consumer_key} = <STDIN>; chomp($setup{consumer_key});
  print "Enter consumer secret: ";
  $setup{consumer_secret} = <STDIN>; chomp($setup{consumer_secret});
}
my $client = Net::Twitter::Lite::WithAPIv1_1->new(%setup);
unless ($client->authorized) {
  print "Not authorised. Approve at ".$client->get_authorization_url." and enter PIN:\n";
  my $pin = <STDIN>;
  chomp($pin);
  my ($token, $token_secret, $user, $nick) = $client->request_access_token(verifier => $pin);
  open(OUT, ">$authFile") or die "Unable to write to $authFile: $!";
  print OUT $setup{consumer_key}."\n".
            $setup{consumer_secret}."\n".
            $token."\n".
            $token_secret."\n";
  close(OUT) or die "Unable to close $authFile: $!";
}

chomp(my $since_id = `cat ~/.archive-tweets-since.$USER 2>/dev/null`);
my $new_since_id = $since_id || 0;

eval {
  my $max_id = 0;
  my $count;
  do {
    my $options = { screen_name => $ARGV[0], include_entities => 1, trim_user => 1, count => 200 };
    $options->{since_id} = $since_id if $since_id;
    $options->{max_id} = $max_id if $max_id;
    my $statuses = $client->user_timeline($options);
    for my $status (@$statuses) {
      $max_id = $status->{id} - 1;
      $new_since_id = $status->{id} if $new_since_id < $status->{id};

      &processTweet($status);
    }
    $count = @$statuses;
    print " $count $max_id $new_since_id\n";
  } until (!$count);
};
warn "$@\n" if $@;

open(OUT, ">$ENV{HOME}/.archive-tweets-since.$USER") or die $!;
print OUT "$new_since_id\n";
close(OUT) or die $!;
exit;


# ---------------------------------------------------------------------
# Save a tweet
sub processTweet {
  my ($tweet) = @_;

  my @time = strptime($tweet->{created_at});
  my $file = strftime('%Y-%m-%dT%H:%M:%S', @time).'.tweet.json';
  print '#';
  open(OUT, ">$DIR/$file") or die "Unable to write to $file: $!\n";
  print OUT JSON::XS->new->utf8->pretty->encode($tweet);
  close(OUT) or die "Unable to close $file: $!\n";
}

