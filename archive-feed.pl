#!/usr/bin/perl
#
# Generate JSON items from RSS/Atom feed.
# Copyright (c) Andrew Flegg 2012. Released under the Artistic Licence
#
# Syntax:
#    archive-feed <RSS feed URL> <dir>

use strict;
use warnings;
use XML::Feed;
use JSON::XS;

# -- Check syntax...
#
our $URL = $ARGV[0] or die "syntax: $0 <RSS feed URL> <dir>\n";
our $DIR = $ARGV[1] or die "syntax: $0 '$URL' <dir>\n";

# -- Get, and parse, file...
#
my $feed = XML::Feed->parse(URI->new($URL)) or die XML::Feed->errstr;
foreach my $item ($feed->entries) {
  my $file = $item->issued()->iso8601().'.rss.json';
  open(OUT, ">$DIR/$file") or die "Unable to write $file: $!\n";
  my %data = ( 'title' => $item->title,
                'link'  => $item->link,
                'content' => { 'body' => $item->content->body, 'type' => $item->content->type },
                'id' => $item->id,
                'created_at' => $item->issued->iso8601(),
                'last_modified' => $item->modified->iso8601() );
  $data{tags} = $item->tags if $item->tags;
  print OUT JSON::XS->new->utf8->pretty->encode(\%data);
  close(OUT) or die "Unable to close: $!\n";
}

